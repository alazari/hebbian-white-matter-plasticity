#!/bin/bash

maptype="MT"
analysisList="physio_active_control_interaction_negative"

cohort=All

for analysis in $analysisList; do

studyDir=/vols/Scratch/Volume/"$cohort"
analysisDir=/vols/Scratch/Volume/"$cohort"/"$analysis"
mkdir -p $analysisDir

cp $studyDir/"$analysis".mat $analysisDir/"$analysis".mat
cp $studyDir/"$analysis".con $analysisDir/"$analysis".con
# cp $studyDir/"$analysis".grp $analysisDir/"$analysis".grp

imcp /vols/Scratch/Volume/MNI152_T1_0.7mm_brain_mask.nii.gz $analysisDir/brain_mask

for map in $maptype; do

imcp $studyDir/all_"$map"maps_diff_s3_4D.nii.gz $analysisDir/all_"$map"maps_diff_s3_4D.nii.gz

done

# set up the stats
inputData=$analysisDir/all_MTmaps_diff_s3_4D.nii.gz
mask=$analysisDir/brain_mask
stat_mat=$analysisDir/"$analysis".mat
stat_con=$analysisDir/"$analysis".con
outDir=$analysisDir/results
mkdir -p $outDir
mkdir -p $outDir/MT

# Volume analysis
randomise_parallel -i $inputData -o $outDir/MT -m $mask -d $stat_mat -t $stat_con -T -n 5000

done
