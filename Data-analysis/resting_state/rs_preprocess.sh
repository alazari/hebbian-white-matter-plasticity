#!/usr/bin/env bash

# definitions
workDir=/vols/Scratch/resting_state

# specify the subjects
subjList=""

for subj in $subjList ; do

  echo "preprocessing resting state for subject $subj"

  # prepare the .fsf file based on the template
  cp $workDir/template.fsf $workDir/$subj/"$subj".fsf
  sed -i "s/XXX/"$subj"/g" $workDir/$subj/"$subj".fsf

  # run the .fsf design on feat
  fsl_sub -q long.q -s openmp,2 feat $workDir/"$subj"/"$subj"

echo "done"

done
