#!/usr/bin/env bash

# definitions
workDir=/vols/Scratch/
fixDir=/vols/Scratch/

# specify the subjects
subjList=""

for subj in $subjList ; do

  echo "applying FIX for resting state on subject $subj"

  fix_classify=` fsl_sub -q veryshort.q $fixDir/fix/fix -c $workDir/"$subj".ica $fixDir/classifier.RData 20 `

  fix_clean=` fsl_sub -q veryshort.q -j ${fix_classify} $fixDir/fix/fix -a $workDir/"$subj".ica/fix4melview_classifier_thr20.txt -m -h -0`

echo "done"

done
