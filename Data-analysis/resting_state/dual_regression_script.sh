#!/bin/bash

# definitions
workDir=/vols/Scratch/resting_state/
scriptDir=/vols/Scratch/resting_state_pipeline
outDir=/vols/Scratch/gICA
mkdir -p $outDir

list=all

dual_regression $outDir/probabilistic_60_IFG_M1_stim_sites 1 -1 1000 \
    $outDir/$list/rIFG_M1_60_probabilistic_smoothing_"$list".dr `cat $scriptDir/"$list"_dirs_smoothed.txt`
