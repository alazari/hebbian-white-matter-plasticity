#!/usr/bin/env bash

# definitions
scriptDir=/vols/Scratch/
workDir=$scriptDir/seeds
mkdir -p $scriptDir/seeds

refImg=$FSLDIR/data/standard/MNI152_T1_2mm_brain

# Create ROIs to analyse

# Left PMd	−22	−4	58
# Left AIP	−44	−42	46
# Left pSPL	−22	−64	54
# Left V3A	−26	−86	18

# PMd
coord=($(echo -22 -4 58| std2imgcoord -img $refImg -std $refImg -vox -))
fslmaths $refImg -mul 0 -add 1 -roi ${coord[0]} 1 ${coord[1]} 1 ${coord[2]} 1 0 -1 $scriptDir/seeds/PMd_human_point
fslmaths $scriptDir/seeds/PMd_human_point -kernel sphere 3 -fmean $scriptDir/seeds/PMd_human_seed -odt float

# AIP
coord=($(echo -44 -42 46 | std2imgcoord -img $refImg -std $refImg -vox -))
fslmaths $refImg -mul 0 -add 1 -roi ${coord[0]} 1 ${coord[1]} 1 ${coord[2]} 1 0 -1 $scriptDir/seeds/lAIP_human_point
fslmaths $scriptDir/seeds/lAIP_human_point -kernel sphere 3 -fmean $scriptDir/seeds/lAIP_human_seed -odt float

# pSPL
coord=($(echo -22 -64 54 | std2imgcoord -img $refImg -std $refImg -vox -))
fslmaths $refImg -mul 0 -add 1 -roi ${coord[0]} 1 ${coord[1]} 1 ${coord[2]} 1 0 -1 $scriptDir/seeds/pSPL_human_point
fslmaths $scriptDir/seeds/pSPL_human_point -kernel sphere 3 -fmean $scriptDir/seeds/pSPL_human_seed -odt float

# V3A
coord=($(echo -26 -86 18 | std2imgcoord -img $refImg -std $refImg -vox -))
fslmaths $refImg -mul 0 -add 1 -roi ${coord[0]} 1 ${coord[1]} 1 ${coord[2]} 1 0 -1 $scriptDir/seeds/V3A_human_point
fslmaths $scriptDir/seeds/V3A_human_point -kernel sphere 3 -fmean $scriptDir/seeds/V3A_human_seed -odt float


rm $scriptDir/seeds/all_seeds.nii.gz
fslmerge -t $scriptDir/seeds/all_seeds $scriptDir/seeds/*_human_seed.nii.gz
fslmaths $scriptDir/seeds/all_seeds -Tmax -bin $scriptDir/seeds/all_seeds
