#!/usr/bin/env bash

list=All
timepoints="pre post"

# definitions
workDir=/vols/Scratch/resting_state
scriptDir=/vols/Scratch/resting_state_pipeline
outDir=/vols/Scratch/gICA
mkdir -p $outDir

for subj in $(cat longitudinal_paTMS_${list}.txt); do

  for time in $timepoints; do

  echo "finding directory for $subj"
  echo "$workDir/"$subj""$time"/registration.feat/filtered_func_data_clean_standard.nii.gz" >> $scriptDir/"$list"_dirs.txt

  done

done


mkdir -p $outDir/$list

# run group melodic:
# automatic
fsl_sub -q verylong.q melodic -i $scriptDir/"$list"_dirs.txt -o $outDir/"$list"/groupICA_"$list" \
    --tr=0.75 --nobet -a concat \
    -m /opt/fmrib/fsl/data/standard/MNI152_T1_2mm_brain \
    --report --Oall

# 30 dimensions
fsl_sub -q verylong.q melodic -i $scriptDir/"$list"_dirs.txt -o $outDir/"$list"/groupICA_30_"$list" \
    --tr=0.75 --nobet -a concat \
    -m /opt/fmrib/fsl/data/standard/MNI152_T1_2mm_brain \
    --report --Oall -d 30

# 60 dimensions
fsl_sub -q verylong.q melodic -i $scriptDir/"$list"_dirs.txt -o $outDir/"$list"/groupICA_60_"$list" \
    --tr=0.75 --nobet -a concat \
    -m /opt/fmrib/fsl/data/standard/MNI152_T1_2mm_brain \
    --report --Oall -d 60
