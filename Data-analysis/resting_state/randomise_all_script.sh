#!/bin/bash
#!/bin/bash

rsDir=/vols/Scratch/resting_state/gICA
studyDir=/vols/Scratch/

analysisList=""
cohort=All

for analysis in $analysisList; do

analysisDir=/vols/Scratch/resting_state/gICA/"$analysis"
mkdir -p $analysisDir

cp $rsDir/"$analysis".mat $analysisDir/"$analysis".mat
cp $rsDir/"$analysis".con $analysisDir/"$analysis".con

imcp /opt/fmrib/fsl/data/standard/MNI152_T1_2mm_brain_mask.nii.gz $analysisDir/brain_mask

imcp $studyDir/statsdiff/all_rs_rIFG_M1_Z_diff $analysisDir/all_rs_rIFG_M1_Z_diff

# set up the stats
mask=$analysisDir/brain_mask
stat_mat=$analysisDir/"$analysis".mat
stat_con=$analysisDir/"$analysis".con
outDir=$analysisDir/results
mkdir -p $outDir

randomise_parallel -i $analysisDir/all_rs_rIFG_M1_Z_diff -o $outDir -m $mask -d $stat_mat -t $stat_con -T -n 1000

done
