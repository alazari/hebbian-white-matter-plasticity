# Project repository for <em> Hebbian activity-dependent plasticity in white matter </em>

Welcome to the project repository for the materials and code accompanying <em>  “Hebbian activity-dependent plasticity in white matter” </em> by Lazari and colleagues (DOI: 10.5281/zenodo.6532370)!

Here, we are aiming to share the following:

1. [Behavioural data acquisition code](https://git.fmrib.ox.ac.uk/alazari/hebbian-white-matter-plasticity/-/tree/master/Data-collection).

1. [Magnetic Resonance Imaging acquisition protocols](https://ora.ox.ac.uk/objects/uuid:e4fb5c17-e55e-4dc4-bc4f-e8246d969a5d).

1. [Data analysis code](https://git.fmrib.ox.ac.uk/alazari/hebbian-white-matter-plasticity/-/tree/master/Data-analysis).

1. [Statistical maps and Neuroanatomical masks](https://git.fmrib.ox.ac.uk/alazari/hebbian-white-matter-plasticity/-/tree/master/Statistical-maps-and-Neuroanatomical-masks).

1. [Other miscellaneous materials](https://git.fmrib.ox.ac.uk/alazari/hebbian-white-matter-plasticity/-/tree/master/Questionnaires) (such as questionnaires used in the study).


We hope you find this useful, and you are welcome to get in touch (alberto.lazari@ndcn.ox.ac.uk) if more information is needed.

